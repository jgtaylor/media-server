use chrono::prelude::*;

use rand::{self};
use std::fmt::Debug;
use std::path::PathBuf;
use symphonia::core::formats::FormatOptions;

use symphonia::core::io::MediaSourceStream;
use symphonia::core::meta::{MetadataLog, MetadataOptions};
use symphonia::core::probe::Hint;

// const probe: Probe = symphonia::default::get_probe();
// static registry: &CodecRegistry = symphonia::default::get_codecs();

#[derive()]
pub struct Movie {
    pub imdb_id: String,
    pub id: u128,
    pub name: String,
    pub release_date: NaiveDate,
    pub path: PathBuf,
    pub _codec: Option<symphonia::core::meta::MetadataRevision>,
    pub container: Option<MetadataLog>,
}

impl Debug for Movie {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Movie")
            .field("name", &self.name)
            .field("path", &self.path)
            .field("id", &self.id)
            .field("imdb_id", &self.imdb_id)
            .field("release_date", &self.release_date)
            .field("Container", &self.container)
            .field("_codec", &self._codec)
            .finish()
    }
}

impl Movie {
    pub fn new(
        imdb_id: String,
        name: String,
        release_date: chrono::NaiveDate,
        path: PathBuf,
    ) -> Self {
        let id = rand::random::<u128>; // TODO: check for the id existing later
        let src = std::fs::File::open(&path).expect("failed to open media");
        let mss = MediaSourceStream::new(Box::new(src), Default::default());
        let hint = Hint::new();
        let meta_opts: MetadataOptions = Default::default();
        let fmt_opts: FormatOptions = Default::default();
        let mut probed = symphonia::default::get_probe()
            .format(&hint, mss, &fmt_opts, &meta_opts)
            .expect("unsupported format");
        let mut _m: Option<symphonia::core::meta::MetadataRevision> =
            probed.format.metadata().current().cloned();

        let container = probed.metadata.into_inner();
        Self {
            id: id(),
            imdb_id,
            name,
            release_date,
            path,
            _codec: _m,
            container,
        }
    }
}
