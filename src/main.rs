use media_server::movies::Movie;

fn main() {
    /*
    pick a web framework and have it do our bidding.
    */
    let my_movie = Movie::new(
        "tt88383".to_string(),
        "the Grench".to_string(),
        chrono::Local::now().date_naive(),
        "/srv/io/media/movies/The Great Outdoors (1988) [imdbid-tt0095253]/The Great Outdoors (1988) [imdbid-tt0095253] - [Bluray-1080p][EAC3 5.1][x265]-iVy.mkv".into(),
    );
    println!("{:#?}", my_movie);
}
